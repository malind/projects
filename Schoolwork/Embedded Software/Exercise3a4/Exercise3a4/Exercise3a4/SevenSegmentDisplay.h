/*
===================================================================
Name          :      SevenSegmentDisplay.cpp
Author        :      Matti Lindholm, Jimi Toiviainen
Description   :      Header-file for SevenSegmentDisplay.cpp
===================================================================
*/

#ifndef SEVENSEGMENTDISPLAY_H_
#define SEVENSEGMENTDISPLAY_H_

void controlLeds(int* ledArray);
void showCharacter(char c);
void drawWithLeds(int* ledArray, int loopCount);

#endif /* SEVENSEGMENTDISPLAY_H_ */