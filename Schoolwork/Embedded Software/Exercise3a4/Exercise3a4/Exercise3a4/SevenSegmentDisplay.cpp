/*
===================================================================
Name          :      SevenSegmentDisplay.cpp
Author        :      Matti Lindholm, Jimi Toiviainen
Description   :      Control functions for a 7-Segment Display
===================================================================
*/

 #include <Arduino.h>
 #include <stdio.h>
 #include "SevenSegmentDisplay.h"

 #define ARRAY_SIZE 7

 // Array as input. Turn LEDs based on index values ON / OFF
 void drawWithLeds(int ledArray[], int loopCount) {
	 float delayTime = 100;
	 for (int loopIndex = 0; loopIndex < loopCount; loopIndex++) {
		 for (int index = 0; index < 6; index++) {
			 digitalWrite(ledArray[index], HIGH);
			 delay(delayTime);
		 }
		 for (int index = 0; index < 6; index++) {
			 digitalWrite(ledArray[index], LOW);
			 delay(delayTime);
		 }
	 }
 }

 // If a value in an array is 1 = LED ON
 // If a value in an array is 0 = LED OFF
 void controlLeds(int ledArray[]) {
	 for (int index = 0; index < ARRAY_SIZE; index++) {
		 if (ledArray[index] == 1) {
			 digitalWrite(index + 2, HIGH);
		 }
		 if (ledArray[index] == 0) {
			 digitalWrite(index + 2, LOW);
		 }
	 }
 }

 // Used to display letters and numbers on the 7-Segment Display
 void showCharacter(char c) {
	 switch (c) {
		 case 'E':
		 {
			 int letterE[] = {1, 1, 1, 1, 0, 1, 0};
			 controlLeds(letterE);
		 }
		 break;
		 case '0':
		 {
			 int numberZero[] = {1, 1, 1, 1, 1, 0, 1};
			 controlLeds(numberZero);
		 }
		 break;
		 case '1':
		 {
			 int numberOne[] = {0, 0, 0, 0, 1, 0, 1};
			 controlLeds(numberOne);
		 }
		 break;
		 case '2':
		 {
			 int numberTwo[] = {1, 0, 1, 1, 0, 1, 1};
			 controlLeds(numberTwo);
		 }
		 break;
		 case '3':
		 {
			 int numberThree[] = {1, 0, 0, 1, 1, 1, 1};
			 controlLeds(numberThree);
		 }
		 break;
		 case '4':
		 {
			 int numberFour[] = {0, 1, 0, 0, 1, 1, 1};
			 controlLeds(numberFour);
		 }
		 break;
		 case '5':
		 {
			 int numberFive[] = {1, 1, 0, 1, 1, 1, 0};
			 controlLeds(numberFive);
		 }
		 break;
		 case '6':
		 {
			 int numberSix[] = {1, 1, 1, 1, 1, 1, 0};
			 controlLeds(numberSix);
		 }
		 break;
		 case '7':
		 {
			 int numberSeven[] = {1, 0, 0, 0, 1, 0, 1};
			 controlLeds(numberSeven);
		 }
		 break;
		 case '8':
		 {
			 int numberEight[] = {1, 1, 1, 1, 1, 1, 1};
			 controlLeds(numberEight);
		 }
		 break;
		 case '9':
		 {
			 int numberNine[] = {1, 1, 0, 1, 1, 1, 1};
			 controlLeds(numberNine);
		 }
		 break;
	 }
 }
