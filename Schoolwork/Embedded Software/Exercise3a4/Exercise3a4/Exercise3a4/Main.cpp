﻿/*
=============================================================================
Name          :      Exercise3a4
Author        :      Matti Lindholm, Jimi Toiviainen
Description   :      Shows letters and numbers, two digits of temperature and 
                     makes clockwise circles when a button is pressed.
=============================================================================
*/

#include <Arduino.h>
#include <stdio.h>
#include "SevenSegmentDisplay.h"

#define THERMISTORPIN A0
#define BUTTON_INPUT 10

float getTemperature(float temperatureReading);
char setTemperatureString(char *temperatureString);

void setup() {
  for (int index = 2; index < 9; index++) {
    pinMode(index, OUTPUT);
  }
  pinMode(BUTTON_INPUT, INPUT);
}

void loop() {
  int buttonState = 0;      // Current state of the button
  int lastButtonState = 0;  // Previous state of the button
  int programState = 0;     
  char temperatureString[] = {'0','0','0','0'};
  
  while (1) {
    buttonState = digitalRead(BUTTON_INPUT);
    int ledsClockwise[] = {2, 8, 6, 5, 4, 3};
    int ledsOff[] = {0, 0, 0, 0, 0, 0, 0};
    // If button state changed
    if (buttonState != lastButtonState) {
      // If the button went on
      if (buttonState == HIGH) {
        lastButtonState = buttonState;
        switch (programState) {
          case (0):
            {
              temperatureString[4] = setTemperatureString(temperatureString);
              // Show E and 3 three times
              for (int index = 0; index < 3; index++) {
                showCharacter('E');
                delay(200);
                showCharacter('3');
                delay(200);
              }
              controlLeds(ledsOff);
              programState++;
            }
            break;
          case (1): 
            {
              showCharacter(temperatureString[0]);
              programState++;
            }
            break;
          case (2):
            {
              showCharacter(temperatureString[1]);
              programState++;
            }
            break;
          case (3):
            {
              controlLeds(ledsOff);
              drawWithLeds(ledsClockwise, 3);
            }
          default:
            {
              controlLeds(ledsOff);
              programState = 0;
            }
          break;
        }
      }
      if (buttonState == LOW) {
        lastButtonState = buttonState;
      }
      // So the button works correctly
      delay(50);
    }
  }
}

// Used to parse the temperature reading to a string
char setTemperatureString(char *temperatureString){
  float thermistorReading = 0;
  thermistorReading = analogRead(THERMISTORPIN);
  int temperatureCelcius = getTemperature(thermistorReading);
  sprintf(temperatureString, "%d", temperatureCelcius);
}

// Returns temperature in Celcius
float getTemperature(float temperatureReading) {
  float temperatureCelcius = 0;
  int lookupTable[][2] = {
    {0, 0}, {250, 14}, {275, 40}, {300, 64}, {325, 88},
    {350, 111}, {375, 134}, {400, 156}, {425, 178}, {450, 200}, 
    {475, 222}, {500, 244}, {525, 267}, {550, 290}, {575, 313}, 
    {600, 337}, {625, 361}, {650, 387}, {675, 413}, {700, 441},
    {725, 471}, {750, 502}, {775, 537}, {784, 550}, {825, 615},
    {850, 662}, {875, 715}, {900, 779}, {925, 857}, {937, 903},
    {950, 960}, {975, 1112}, {1000, 1395}
  };
  int lookupTableSize = sizeof(lookupTable) / sizeof(lookupTable[0]);
  for (int index = 0; index <= lookupTableSize; index++){
    if (lookupTable[index][0] >= temperatureReading){
      temperatureCelcius = lookupTable[index - 1][1] + ((lookupTable[index][1] - lookupTable[index - 1][1]) * ((temperatureReading - lookupTable[index - 1][0]) / (lookupTable[index][0] - lookupTable[index - 1][0])));
      return temperatureCelcius;
    }
  }
}

