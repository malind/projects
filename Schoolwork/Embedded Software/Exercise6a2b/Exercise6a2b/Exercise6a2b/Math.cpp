/*
=============================================================================
Name          :      Math.cpp
Author        :      Matti Lindholm, Jimi Toiviainen
Description   :		 Math-module for Exercise6a2b

=============================================================================
*/

#include "Math.h"

float celsiusToKelvin(int temperature) {
	return (temperature + 273.15);
}

