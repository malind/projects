﻿/*
=====================================================================================
Name          :      Exercise6a2b
Author        :      Matti Lindholm, Jimi Toiviainen
Description   :		 Check temperature. If temperature is lower than 28 in Celcius,
					 then convert the temperature to Kelvins and blink them with LEDs
					 (Green blinks hundreds, yellow blinks tens, red blinks ones).
					 If temperature is equal to 28 or greater in Celcius, then
					 check thermistor voltage. Red blinks hundreds, yellow blinks 
					 tens, green blinks ones. Program ends by turning all LEDs on 
					 for 2 seconds and then off

=====================================================================================
*/

#include <Arduino.h>
#include "Led.h"
#include "Thermistor.h"
#include "Math.h"

// Changing float to int with rounding
#define FLOAT_TO_INT(x) ((x)>=0?(int)((x)+0.5):(int)((x)-0.5))

#define THERMISTORPIN A0
#define RED 3
#define YELLOW 11
#define GREEN 12

void setup() {
  pinMode(RED, OUTPUT);
  pinMode(YELLOW, OUTPUT);
  pinMode(GREEN, OUTPUT);
}

void loop() {
  int temperatureCelsius = getTemperature(THERMISTORPIN);
  if (temperatureCelsius < 28) {
    int temperatureKelvin = FLOAT_TO_INT(celsiusToKelvin(temperatureCelsius));
    // Parse temperature to char array
    char temperatureString[3] = {'0', '0', '0'};
    sprintf(temperatureString, "%d", temperatureKelvin);
    blinkReading(GREEN, temperatureString[0]);
    blinkReading(YELLOW, temperatureString[1]);
    blinkReading(RED, temperatureString[2]);
  }
  else {
    int thermistorReading = FLOAT_TO_INT(analogRead(THERMISTORPIN));
    // Parse temperature to char array
    char voltageString[3] = {'0', '0', '0'};
    sprintf(voltageString, "%d", thermistorReading);
    blinkReading(RED, voltageString[0]);
    blinkReading(YELLOW, voltageString[1]);
    blinkReading(GREEN, voltageString[2]);
  }
  ledsOnOff(2000, RED, YELLOW, GREEN);
}





