/*
=============================================================================
Name          :      Led.cpp
Author        :      Matti Lindholm, Jimi Toiviainen
Description   :		 Led-module for Exercise6a2b

=============================================================================
*/

#include <Arduino.h>
#include "Led.h"

void blinkReading(int led, char reading) {
	for (int index = 0; index < (int)reading - 48; index++) {
		digitalWrite(led, 1);
		delay(250);
		digitalWrite(led, 0);
		delay(250);
	}
	delay(1000);
}

void ledsOnOff(int onTime, int firstLed, int secondLed, int thirdLed) {
	digitalWrite(firstLed, 1);
	digitalWrite(secondLed, 1);
	digitalWrite(thirdLed, 1);
	delay(onTime);
	digitalWrite(firstLed, 0);
	digitalWrite(secondLed, 0);
	digitalWrite(thirdLed, 0);
	delay(100);
}