/*
=============================================================================
Name          :      Thermistor.h
Author        :      Matti Lindholm, Jimi Toiviainen
Description   :		 Header-file for Thermistor.cpp

=============================================================================
*/


#ifndef THERMISTOR_H_
#define THERMISTOR_H_

float getTemperature(int pin);

#endif /* THERMISTOR_H_ */