/*
=============================================================================
Name          :      Led.h
Author        :      Matti Lindholm, Jimi Toiviainen
Description   :		 Header-file for Led.cpp

=============================================================================
*/


#ifndef LED_H_
#define LED_H_

void blinkReading(int led, char reading);
void ledsOnOff(int onTime, int firstLed, int secondLed, int thirdLed);

#endif /* LED_H_ */