/*
=============================================================================
Name          :      Math.h
Author        :      Matti Lindholm, Jimi Toiviainen
Description   :		 Header-file for Math.cpp

=============================================================================
*/


#ifndef MATH_H_
#define MATH_H_

float celsiusToKelvin(int temperature);

#endif /* MATH_H_ */