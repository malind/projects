/*
=============================================================================
Name          :      Thermistor.cpp
Author        :      Matti Lindholm, Jimi Toiviainen
Description   :		 Thermistor-module for Exercise6a2b

=============================================================================
*/

#include <Arduino.h>
#include "Thermistor.h"

// Returns temperature in Celcius
float getTemperature(int pin) {
	float thermistorReading = analogRead(pin);
	float temperatureCelcius = 0;
	int lookupTable[][2] = {
		{0, 0}, {250, 14}, {275, 40}, {300, 64}, {325, 88},
		{350, 111}, {375, 134}, {400, 156}, {425, 178}, {450, 200},
		{475, 222}, {500, 244}, {525, 267}, {550, 290}, {575, 313},
		{600, 337}, {625, 361}, {650, 387}, {675, 413}, {700, 441},
		{725, 471}, {750, 502}, {775, 537}, {784, 550}, {825, 615},
		{850, 662}, {875, 715}, {900, 779}, {925, 857}, {937, 903},
		{950, 960}, {975, 1112}, {1000, 1395}
	};
	int lookupTableSize = sizeof(lookupTable) / sizeof(lookupTable[0]);
	for (int index = 0; index <= lookupTableSize; index++){
		if (lookupTable[index][0] >= thermistorReading){
			temperatureCelcius = lookupTable[index - 1][1] + ((lookupTable[index][1] - lookupTable[index - 1][1]) * ((thermistorReading - lookupTable[index - 1][0]) / (lookupTable[index][0] - lookupTable[index - 1][0])));
			// Divided by 10 because lookup table gives celcius multiplied by 10
			return temperatureCelcius / 10;
		}
	}
	// Return a placeholder value if the reading is outside the lookup table
	return 9999;
}