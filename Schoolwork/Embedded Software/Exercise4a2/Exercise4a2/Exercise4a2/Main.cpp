﻿/*
=============================================================================
Name          :      Exercise4a2
Author        :      Matti Lindholm, Jimi Toiviainen
Description   :      Control LEDs using interrupts                 
=============================================================================
*/

#include <Arduino.h>

#define GREEN 2
#define YELLOW 3
#define RED_ONE 8
#define RED_TWO 9

void blinkWithGreenLED();
void blinkWithYellowLED();

void setup() {
  pinMode(GREEN, OUTPUT);
  pinMode(YELLOW, OUTPUT);
  pinMode(RED_ONE, OUTPUT);
  pinMode(RED_TWO, OUTPUT);
  attachInterrupt(digitalPinToInterrupt(GREEN), blinkWithGreenLED, CHANGE); 
  attachInterrupt(digitalPinToInterrupt(YELLOW), blinkWithYellowLED, CHANGE); 
}

void loop() {  
  digitalWrite(GREEN, HIGH);
  digitalWrite(YELLOW, HIGH);
  delay(500);
  digitalWrite(YELLOW, LOW);
  delay(500);

  digitalWrite(GREEN, LOW);
  digitalWrite(YELLOW, HIGH);
  delay(500);
  digitalWrite(YELLOW, LOW);
  delay(500);
}

// Red LED's state corresponds to green LED's state
void blinkWithGreenLED() {
  digitalWrite(RED_ONE, digitalRead(GREEN));  
}

// Red LED's state is opposite to yellow LED's state
void blinkWithYellowLED() {
  digitalWrite(RED_TWO, !digitalRead(YELLOW));  
}
