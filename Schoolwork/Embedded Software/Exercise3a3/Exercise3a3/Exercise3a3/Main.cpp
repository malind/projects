﻿/*
===================================================================
Name          :      Exercise3a3
Author        :      Matti Lindholm, Jimi Toiviainen
Description   :      Show room temperature on the 7-Segment Display
                     using a 1k thermistor
===================================================================
*/

#include <stdio.h>
#include <Arduino.h>

#include "SevenSegmentDisplay.h"

#define THERMISTORPIN A0
#define NO_READING 99999

float getTemperature(float temperatureReading);

void setup() {
  for (int index = 2; index <= 9; index++) {
    pinMode(index, OUTPUT);
  }
}

void loop() {
  int ledsOff[] = {0, 0, 0, 0, 0, 0, 0, 0};
  float thermistorReading = 0;
  thermistorReading = analogRead(THERMISTORPIN);
  volatile int temperatureCelcius = getTemperature(thermistorReading);
  
  // If we don't have a temperature reading, show dot
  if (temperatureCelcius == NO_READING) {
    digitalWrite(9, HIGH);
    delay(2000);
  }
  else {
    // Parse temperature to char array
    char temperatureString[4] = {'0', '0', '0', '0'};
    sprintf(temperatureString, "%d", temperatureCelcius);
    showTemperature(temperatureString, ledsOff);
    controlLeds(ledsOff);
    delay(300);
  }
}

// Returns temperature in Celcius
float getTemperature(float temperatureReading) {
  float temperatureCelcius = 0;
  int lookupTable[][2] = {
    {0, 0}, {250, 14}, {275, 40}, {300, 64}, {325, 88},
    {350, 111}, {375, 134}, {400, 156}, {425, 178}, {450, 200}, 
    {475, 222}, {500, 244}, {525, 267}, {550, 290}, {575, 313}, 
    {600, 337}, {625, 361}, {650, 387}, {675, 413}, {700, 441},
    {725, 471}, {750, 502}, {775, 537}, {784, 550}, {825, 615},
    {850, 662}, {875, 715}, {900, 779}, {925, 857}, {937, 903},
    {950, 960}, {975, 1112}, {1000, 1395}
  };
  int lookupTableSize = sizeof(lookupTable) / sizeof(lookupTable[0]);
  for (int index = 0; index <= lookupTableSize; index++){
    if (lookupTable[index][0] >= temperatureReading){
      temperatureCelcius = lookupTable[index - 1][1] + ((lookupTable[index][1] - lookupTable[index - 1][1]) * ((temperatureReading - lookupTable[index - 1][0]) / (lookupTable[index][0] - lookupTable[index - 1][0])));
      return temperatureCelcius;
    }
  }
  // Return a placeholder value if the reading is outside the lookup table
  return NO_READING;
}


