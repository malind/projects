/*
===================================================================
Name          :      SevenSegmentDisplay.cpp
Author        :      Matti Lindholm, Jimi Toiviainen
Description   :      Header-file for SevenSegmentDisplay.cpp
===================================================================
*/

#ifndef SEVENSEGMENTDISPLAY_H_
#define SEVENSEGMENTDISPLAY_H_

void showTemperature(char temperatureString[], int ledsOff[]);
void controlLeds(int* ledArray);
void showCharacter(char c);

#endif /* SEVENSEGMENTDISPLAY_H_ */