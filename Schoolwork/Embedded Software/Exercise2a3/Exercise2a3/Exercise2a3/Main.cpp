﻿/*
==============================================================
Name          :      Exercise2a3
Author        :      Matti Lindholm, Jimi Toiviainen
Description   :      Draw letters, numbers and circles on a 7-Segment Display.
                     The program speeds up and slows down.
==============================================================
*/

#include <Arduino.h>

#define ARRAY_SIZE 7

void drawWithLeds(int* ledArray, int loopCount);
void triggerLeds(int loopCount);
void controlLeds(int* ledArray, float delayTime);
void showCharacter(char c);
void controlSpeed();

// Initialize global variables for speed
float delayDelta = -0.24;
float delayMultiplier = 1;

// Setup program which is run once
// Set pins for the 7-Segment Display LEDs
void setup() {
  for (int index = 2; index <= 8; index++) {
    pinMode(index, OUTPUT);
  }
  pinMode(LED_BUILTIN, OUTPUT);
}

// Main program which runs in an infinite loop
void loop() {
  int ledsOff[] = {0, 0, 0, 0, 0, 0, 0};
  int ledsClockwise[] = {2, 8, 6, 5, 4, 3};
  int ledsCounterClockwise[] = {3, 4, 5, 6, 8, 2};
  // Show character E and number 3 three times
  for (int index = 0; index < 3; index++) {
    showCharacter('E');
    showCharacter('3');
  }
  // Turn off all LEDs
  controlLeds(ledsOff, 250 * delayMultiplier);
  drawWithLeds(ledsCounterClockwise, 3);
  controlLeds(ledsOff, 250 * delayMultiplier);
  triggerLeds(3);
  drawWithLeds(ledsClockwise, 3);
  controlLeds(ledsOff, 250 * delayMultiplier);
  controlSpeed();
}

// Controls speed direction using global variables
void controlSpeed() {
  if (delayMultiplier > 1 || delayMultiplier < 0.25) {
    delayDelta = delayDelta * -1;
  }
  // Increase / decrease delays
  delayMultiplier = delayMultiplier + delayDelta;  
}

// Used to draw circles
void drawWithLeds(int ledArray[], int loopCount) {
  
  float delayTime = 100 * delayMultiplier;
  
  for (int loopIndex = 0; loopIndex < loopCount; loopIndex++) {
    for (int index = 0; index < 6; index++) {
      digitalWrite(ledArray[index], HIGH);
      delay(delayTime);
    }
    for (int index = 0; index < 6; index++) {
      digitalWrite(ledArray[index], LOW);
      delay(delayTime);
    } 
  }
}

// Turns all LEDs on and off one by one
void triggerLeds(int loopCount) {
  for (int loopIndex = 0; loopIndex < loopCount; loopIndex++) {
  int ledArray[] = {0, 0, 0, 0, 0, 0, 0};
    for (int index = 0; index < ARRAY_SIZE; index++) {
      ledArray[index] = 1;
      controlLeds(ledArray, 250 * delayMultiplier);
    }
    for (int index = 0; index < ARRAY_SIZE; index++) {
      ledArray[index] = 0;
      controlLeds(ledArray, 250 * delayMultiplier);
    }
  }
}

// Turns all LEDs on and off one by one
// If a value in an array is 1 = LED ON
// If a value in an array is 0 = LED OFF
void controlLeds(int ledArray[], float delayTime) {
  for (int index = 0; index < ARRAY_SIZE; index++) {
    if (ledArray[index] == 1) {
      digitalWrite(index + 2, HIGH);
    }
    if (ledArray[index] == 0) {
      digitalWrite(index + 2, LOW);
    }
  }
  delay(delayTime);
}

// Shows two different characters: E and the number 3
void showCharacter(char c) {
  switch (c) {
    case 'E':
      {
        int letterE[] = {1, 1, 1, 1, 0, 1, 0};
        controlLeds(letterE, 250 * delayMultiplier);
      }
      break;
    case '3':
      {
        int numberThree[] = {1, 0, 0, 1, 1, 1, 1};
        controlLeds(numberThree, 250 * delayMultiplier);
      }
      break;
  }
}
