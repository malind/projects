/*
=============================================================================
Name          :      Led.h of Exercise5a2
Author        :      Matti Lindholm, Jimi Toiviainen
Description   :      LED module header-file
                                        
=============================================================================
*/

#ifndef LED_H_
#define LED_H_

void ledBlink(int led, int delayTime);
void ledOn(int led);
void ledOff();

#endif /* LED_H_ */