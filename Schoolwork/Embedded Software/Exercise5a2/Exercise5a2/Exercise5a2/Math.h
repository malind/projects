/*
=============================================================================
Name          :      Math.h of Exercise5a2
Author        :      Matti Lindholm, Jimi Toiviainen
Description   :      Math module header-file
                                        
=============================================================================
*/

#ifndef MATH_H_
#define MATH_H_

int checkBit(int voltage);

#endif /* MATH_H_ */