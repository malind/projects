/*
=============================================================================
Name          :      Thermistor.h of Exercise5a2
Author        :      Matti Lindholm, Jimi Toiviainen
Description   :      Thermistor module header-file
                                        
=============================================================================
*/


#ifndef THERMISTOR_H_
#define THERMISTOR_H_

float getTemperature(int pin);
int getVoltage(int pin);

#endif /* THERMISTOR_H_ */