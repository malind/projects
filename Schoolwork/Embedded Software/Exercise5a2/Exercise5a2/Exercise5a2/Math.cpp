/*
=============================================================================
Name          :      Math.cpp of Exercise5a2
Author        :      Matti Lindholm, Jimi Toiviainen
Description   :      Math module
                                        
=============================================================================
*/

#include "Math.h"

int checkBit(int voltage) {
	// Least significant bit always determines if the number is even or odd
	// Runtime maybe faster with bitwise operations?
	int leastSignificantBit = voltage % 2;
	return leastSignificantBit;
}
