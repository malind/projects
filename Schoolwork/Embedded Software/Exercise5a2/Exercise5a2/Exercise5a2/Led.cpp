/*
=============================================================================
Name          :      Led.cpp of Exercise5a2
Author        :      Matti Lindholm, Jimi Toiviainen
Description   :      LED module
                                        
=============================================================================
*/

#include "Arduino.h"
#include "Led.h"

void ledBlink(int led, int delayTime) {
	for (int i = 0; i < 5; i++) {
		digitalWrite(led, 1);
		delay(delayTime);
		digitalWrite(led, 0);
		delay(delayTime);
	}
}

void ledOn(int led) {
	ledOff();
	digitalWrite(led, 1);
	delay(1000);
}

void ledOff(){
	for (int index = 6; index <= 8; index++) {
		digitalWrite(index, 0);
	}
}