﻿/*
===============================================================================
Name          :      Exercise5a2
Author        :      Matti Lindholm, Jimi Toiviainen
Description   :      A program which shows the red LED if temperature is >= 25.
					 and green if it's < 25. Yellow LED will blink between 1
					 second intervals if the LSB of thermistor voltage value is
					 1, otherwise it will blink between every 1/5 seconds       
					          
===============================================================================
*/

#include <Arduino.h>
#include "Led.h"
#include "Thermistor.h"
#include "Math.h"

#define THERMISTOR A0
#define GREEN 7
#define YELLOW 6
#define RED 8

// The interval in which the buzzer makes a sound
#define DELAY 5

ISR(TIMER1_OVF_vect);

// Use timer to increment this so we can get longer times than 1 second
static volatile int seconds = 0;

void setup() {
  pinMode(GREEN, OUTPUT);
  pinMode(YELLOW, OUTPUT);
  pinMode(RED, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  
  // Initialize Timer1 and disable all interrupts
  noInterrupts();

  // Set timer registers
  TCCR1A = 0;
  TCCR1B = 0;

  // << and >> operators cause the bits in the left operand to be shifted left or right by 
  // the number of positions specified by the right operand
  // |= Compoundbitwiseor

  TCCR1B |= (1 << CS12);    // Set the prescaler

  // Enable timer overflow interrupt by
  // setting the TOIE1 bit in the TIMSK1 register
  // which enables the timer 1 overflow interrupt from memory
  TIMSK1 |= (1 << TOIE1);
  interrupts();             // Enable all interrupts
}

ISR(TIMER1_OVF_vect) {
  seconds++;
  if (seconds == DELAY) {
    seconds = 0;
    digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
  }
}

void loop() {
  float temperature = getTemperature(THERMISTOR);
  
  if (temperature < 250) {
    ledOn(GREEN);
  } 
  else {
    ledOn(RED);
  }

  ledOff();

  int voltage = getVoltage(THERMISTOR);
  int voltageBit = checkBit(voltage);
  
  if (voltageBit) {
    ledBlink(YELLOW, 1000);
  } 
  else {
    ledBlink(YELLOW, 200);
  }
}

