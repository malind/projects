﻿/*
=============================================================================
Name          :      Exercise4a3
Author        :      Matti Lindholm, Jimi Toiviainen
Description   :      Pedestrian traffic lights. Made with LEDs, button and
                     interrupts                   
=============================================================================
*/

#include <Arduino.h>

#define BUTTON_INPUT 2
#define GREEN 3
#define YELLOW 4
#define RED 9

volatile int loopExecute = 0;

void executeLoop();
void changeTrafficLights();

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(RED, OUTPUT);
  pinMode(YELLOW, OUTPUT);
  pinMode(GREEN, OUTPUT);
  attachInterrupt(digitalPinToInterrupt(BUTTON_INPUT), executeLoop, CHANGE); 
}

void loop() {
  // Red light on
  digitalWrite(RED, HIGH);
  while (loopExecute) {
      changeTrafficLights();
  }
}

void executeLoop() {
  loopExecute = 1;
}

void changeTrafficLights(){
  digitalWrite(LED_BUILTIN, HIGH);
  // Red light on for 3 seconds
  digitalWrite(RED, HIGH);
  delay(3000);

  // Yellow light on with red light for 1 more second
  digitalWrite(YELLOW, HIGH);
  delay(1000);

  // Red and yellow lights off and green light on for 3 seconds
  digitalWrite(RED, LOW);
  digitalWrite(YELLOW, LOW);
  digitalWrite(GREEN, HIGH);
  digitalWrite(LED_BUILTIN, LOW);
  delay(3000);
  
  // Green light off and yellow light on for 1 second
  digitalWrite(GREEN, LOW);
  digitalWrite(YELLOW, HIGH);
  delay(1000);
  // Yellow light off when program is complete
  digitalWrite(YELLOW, LOW);
  loopExecute = 0;
}
