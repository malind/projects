/*
==============================================================================
Name          :      Thermistor.h
Author        :      Matti Lindholm, Jimi Toiviainen
Description   :		 Header-file for Thermistor.h

==============================================================================
*/

#ifndef THERMISTOR_H_
#define THERMISTOR_H_

float getTemperature(int pin);
void showTemperature(int temperature);

#endif /* THERMISTOR_H_ */