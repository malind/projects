/*
==============================================================================
Name          :      Math.cpp
Author        :      Matti Lindholm, Jimi Toiviainen
Description   :      Math-module for Exercise6a2a

==============================================================================
*/

 #include "Math.h"

 int isPrime(int number){
	 // No reason to check further than value / 2. Numbers above that couldn't divide evenly
	 for (int index = 2; index <= number/2; index++) {
		 if (number % index == 0) {
			 return 0;
		 }
	 }
	 return 1;
 }
