/*
==============================================================================
Name          :      Math.h
Author        :      Matti Lindholm, Jimi Toiviainen
Description   :		 Header-file for Math.cpp

==============================================================================
*/


#ifndef MATH_H_
#define MATH_H_

int isPrime(int number);

#endif /* MATH_H_ */