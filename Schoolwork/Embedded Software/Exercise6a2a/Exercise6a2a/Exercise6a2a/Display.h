/*
==============================================================================
Name          :      Display.h
Author        :      Matti Lindholm, Jimi Toiviainen
Description   :		 Header-file for Display.cpp

==============================================================================
*/

#ifndef DISPLAY_H_
#define DISPLAY_H_

void controlLeds(int ledArray[]);
void showCharacter(char c);

#endif /* DISPLAY_H_ */