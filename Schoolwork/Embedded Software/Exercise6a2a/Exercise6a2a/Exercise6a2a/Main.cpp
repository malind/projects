﻿/*
==============================================================================
Name          :      Exercise6a2a
Author        :      Matti Lindholm, Jimi Toiviainen
Description   :      Button press returns celcius. If temperature is prime the
					 program will display it on a 7-Segment Display. If not,
					 a red LED will be blinked 3 times
					              
==============================================================================
*/

#include <Arduino.h>

#include "Display.h"
#include "Math.h"
#include "Thermistor.h"

// Changing float to int with rounding
#define FLOAT_TO_INT(x) ((x)>=0?(int)((x)+0.5):(int)((x)-0.5))

#define THERMISTORPIN A0

#define BUTTON_INPUT 2
#define RED 3

volatile int loopExecute = 0;

void blinkRedLed(int delayTime);
void executeLoop();

void setup() {
	pinMode(RED, OUTPUT);
	pinMode(BUTTON_INPUT, INPUT);
	for (int index = 4; index <= 10; index++) {
		pinMode(index, OUTPUT);
	}
	attachInterrupt(digitalPinToInterrupt(BUTTON_INPUT), executeLoop, HIGH);
}

void loop() {
	digitalWrite(RED, HIGH);
	// Button press triggers interrupt
	while (loopExecute) {
		int temperature = FLOAT_TO_INT(getTemperature(THERMISTORPIN));
		if (isPrime(temperature)) {
			showTemperature(temperature);
		}
		else {
			blinkRedLed(250);
		}
		loopExecute = 0;
	}
}

void executeLoop() {
	loopExecute = 1;
}


void blinkRedLed(int delayTime){
	for (int index = 0; index < 3; index++) {
		digitalWrite(RED, HIGH);
		delay(delayTime);
		digitalWrite(RED, LOW);
		delay(delayTime);
	}
}
