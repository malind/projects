/*
Author: 	Toni Huovinen, Matti Lindholm
File name:	Assignment1.c
Description:	Three functions, first generates random number between 0-1000. Second function returns the sum
				of odd numbers between 0 and random number. Third function checks if the random number is part 			
				of the Tetranacci-sequence.
*/

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

// Function declaration
int randomNumber();
int sumOfOdd(int randomNumber);
void isTetranacci(int sumOfNumbers);

int main () {
	int random = randomNumber();
	printf("Random number: %d\n", random);
	int sum = sumOfOdd(random);
	printf("Sum of odd numbers: %d\n", sum);
	isTetranacci(sum);
	return 0;
}

int randomNumber(){
	// Initialize local variable
	int randomNumber = 0;
	// Use time as seed
	srand(time(NULL));
	// Create random value, starts from 0, cap it at 1000
	randomNumber = rand() % 1001;
	return randomNumber;
}

int sumOfOdd(int randomNumber){
	// Initialize local variable
	int sumOfNumbers = 0;
	// Pass in the randomNumber as loop length
	for (int i = 1; i <= randomNumber; i++){
		// Using modulus. If i is not divisible by 2, then it is odd and proceed to summation
		if (i % 2 != 0){
			sumOfNumbers = sumOfNumbers + i;
		}
	}
	return sumOfNumbers;
}

void isTetranacci(int sumOfNumbers){
	// Initializes the elements of Tetranacci sequence
	int tetraA = 0;
	int tetraB = 0;
	int tetraC = 0;
	int tetraD = 1;
	int tetraSum = 0;
	// Exits loop when when tetraSum is either equal or something else than sumOfNumbers
	// Iterates through variables, passing values one by one forward
	while(tetraSum < sumOfNumbers){
		tetraSum = tetraA + tetraB + tetraC + tetraD;
		tetraA = tetraB;
		tetraB = tetraC;
		tetraC = tetraD;
		tetraD = tetraSum;
	}
	// If it is equal, then it is part of sequence
	if (tetraSum == sumOfNumbers){
		printf("Number %d is Tetranacci\n", sumOfNumbers);
	}
	else{
		printf("Number %d is not Tetranacci\n", sumOfNumbers);
	}
}
