/*
======================================================================================
Filename:		Plane.c
Authors:		Toni Huovinen, Matti Lindholm
Description:	Holds the functions for main(). With these you can initialize struct of any size,
				print out the information of stored planes and you can send created structs to array
======================================================================================
*/

#include <stdio.h>
#include <string.h>
#include "Plane.h"

void initializeStructArray() {
	for(int i = 0; i < 5; i++) {
		strcpy(planeArray[i].name, "NULL");
		planeArray[i].seats = -1;
		planeArray[i].wingSpan = -1;
	}
}


void printPlanes() {
	for (int i = 0; i < 5; i++) {
		printf("Plane %d information:\n", i + 1);
		printf("Name:\t\t%s\n", planeArray[i].name);
		printf("Seats:\t\t%d\n", planeArray[i].seats);
		printf("Wing Span:\t%.1f\n", planeArray[i].wingSpan);
		printf("\n");
	}
}


void sendToArray(char tempName[], int tempSeats, float tempWingSpan, int pos) {
	strncpy(planeArray[pos].name, tempName, 20);
	planeArray[pos].seats = tempSeats;
	planeArray[pos].wingSpan = tempWingSpan;
}
