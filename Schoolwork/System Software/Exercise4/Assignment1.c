/*
======================================================================================
Filename:		Assignment1.c
Authors:		Toni Huovinen, Matti Lindholm
Description:	This program asks the user information about airplanes. Information is
				stored in a struct array, and printed out
======================================================================================
*/

#include <stdio.h>
#include <string.h>
#include "Plane.h"

#define MAX 5

int main() {
	int userDefinedInput = 0;
	int lengthOfInput = 0;
	char tempName[20];
	int tempSeats = 0;
	float tempWingSpan = 0;
	initializeStructArray();
	printf("Enter the number of planes you wish to store (max %d):\n", MAX);
	scanf("%d", &userDefinedInput);
	if(userDefinedInput > MAX) {
		printf("You can only store %d planes. Setting the input as %d\n", MAX, MAX);
		userDefinedInput = MAX;
	}
	for(int k = 0; k < userDefinedInput; k++) {
		printf("Enter the model of plane %d\n", k+1);
		// Clears the buffer from userDefinedInput 'newline'. Without this
		// program jumps over the fgets()
		fgetc(stdin);
		fgets(tempName, 20, stdin);
		// Gets rid of the new line that comes from using fgets() function
		// Checks the length of input, and if it ends with \n, replaces it with 0
		lengthOfInput = strlen(tempName);
		if(tempName[lengthOfInput - 1] == '\n') {
			tempName[lengthOfInput - 1] = 0;
		}
		printf("How many seats the plane has\n");
		scanf("%d", &tempSeats);
		printf("Wing span of the plane (in meters)\n");
		scanf("%f", &tempWingSpan);
		sendToArray(tempName, tempSeats, tempWingSpan, k);
	}
	printPlanes();	
	return 0;
}


