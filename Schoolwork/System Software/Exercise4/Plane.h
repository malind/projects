/*
 =========================================================================================
 Name        : Plane.h
 Author      : Toni Huovinen, Matti Lindholm
 Description : Holds the relevant information for functions and has a declaration of struct
 =========================================================================================
 */


#ifndef PLANE_H
#define PLANE_H

#define SIZE 5


struct planes {

	char name[20];
	int seats;
	float wingSpan;
};

struct planes planeArray[SIZE];

void initializeStructArray();
void printPlanes();
void sendToArray(char tempName[], int tempSeats, float tempWingSpan, int pos);


#endif