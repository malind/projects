/*
 ============================================================================
 Name        : Conversions.h
 Author      : Matti Lindholm, Toni Huovinen
 Description : Header-file for Conversions.c
 ============================================================================
*/

#ifndef CONVERSIONS_H
#define CONVERSIONS_H

void Print(int intArray[], int indexStart);
int BinaryToDeci(int intArray[], int indexStart);
void DeciToHex(int result);

#endif