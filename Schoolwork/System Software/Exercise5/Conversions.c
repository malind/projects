/*
 ============================================================================
 Name        : Conversions.c
 Author      : Matti Lindholm, Toni Huovinen
 Description : Print and conversion functions for Exercise 5
 ============================================================================
*/

#include <stdio.h>
#include <string.h>
#include <math.h>
#include "Conversions.h"

void Print(int intArray[], int indexStart) {
	printf("Binary: ");
	for (int i = indexStart; i < (indexStart+8); i++) {
		printf("%d", intArray[i]);
	}
	printf("\n");
}

// Binary -> Decimal
int BinaryToDeci(int intArray[], int indexStart) {
	int result = 0;
	int powerOf = 0;
	// Convert binary to decimal
	for (int i = (indexStart-1); i >= (indexStart-8); i--) {
		result += (intArray[i] * pow(2, powerOf));
		powerOf++;
	}
	printf("Decimal: %d\n", result);
	return result;
}

// Decimal -> Hexadecimal
void DeciToHex(int decimal) {
	int remainder = 0;
	int i = 0; 
	int indexCounter = 0;
	char hexArray[2];
	// Convert decimal to hexadecimal
	while (decimal != 0) {
		remainder = decimal % 16;
		// Use ASCII for the conversion
		// Numbers
		if (remainder < 10) {
			hexArray[indexCounter++] = 48 + remainder;
		}
		// Letters
		else {
			hexArray[indexCounter++] = 55 + remainder;			
		}
		decimal = decimal / 16;
	}
	printf("Hex: ");
	// Print the results
	for (i = indexCounter-1; i >= 0; i--) {
		printf("%c", hexArray[i]);
	}
	printf("\n");
}