/*
 ============================================================================
 Name        : Exercise5.c
 Author      : Matti Lindholm, Toni Huovinen
 Description : Read user input as command line arguments, parse the binary numbers
 	       and convert them into decimal and hexadecimal values
 ============================================================================
*/

#include <stdio.h>
#include <string.h>
#include <math.h>
#include "Conversions.h"

int main(int argc, char *argv[]) {
	int i = 0;
	int counter = 0;
	int arrLength = 0;
	if (argc > 1) {
		// Define the length for the new array(s)
		for (i = 1; i < argc; i++) {
			arrLength += strlen(argv[i]);
		}
		int intArray[arrLength];
		// Copy the argv[1] into a char array
		char allChars[arrLength];
		strcpy(allChars, argv[1]);
		// Add the rest of the arguments that follow
		for (i = 2; i < argc; i++) {
			strcat(allChars, argv[i]);
		}
		// Copy binary numbers given with arguments to an int array
		for (i = 0; i < arrLength; i++) {
			if (allChars[i] == '0' || allChars[i] == '1') {
				intArray[counter] = (allChars[i] - '0');
				counter++;
			}	
		}
		// Check if the amount of binary numbers is divisible by 8
		// Also make sure that the input is not 0 or empty
		if (counter != 0 && counter % 8 == 0) {
			int startIndex = 0;
			printf("\n");
			for (int index = counter; index > 0; index -= 8) {
				Print(intArray, startIndex);
				DeciToHex(BinaryToDeci(intArray, (startIndex+8)));
				startIndex += 8;
				printf("\n");
			}
		}
		else {
			printf("Input ERROR. You MUST input binary numbers. The count of numbers must be DIVISIBLE by 8!\n");
		}
	}
	else {
		printf("Input ERROR! The input CANNOT BE empty!\n");
	}
	return 0; 
}