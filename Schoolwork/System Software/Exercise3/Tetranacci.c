/*
 ============================================================================
 Name        : Tetranacci.c
 Author      : Toni Huovinen, Matti Lindholm
 Description : Value from array is checked if it is part of Tetranacci series
 ============================================================================
 */



#include <stdio.h>
#include "Tetranacci.h"


int isTetranacci(int valueFromArray){
	// Initializes the elements of Tetranacci sequence
	int tetraA = 0;
	int tetraB = 0;
	int tetraC = 0;
	int tetraD = 1;
	int tetraSum = 0;
	// Exits loop when when tetraSum is either equal or something else than valueFromArray
	// Iterates through variables, passing values one by one forward
	while(tetraSum < valueFromArray){
		tetraSum = tetraA + tetraB + tetraC + tetraD;
		tetraA = tetraB;
		tetraB = tetraC;
		tetraC = tetraD;
		tetraD = tetraSum;
	}
	// If it is equal, then it is part of sequence and returns 1
	// which is essentially true. If it is not equal, then returns 0 which is false.
	if (tetraSum == valueFromArray){
		return 1;
	}
	else{
		return 0;
	}
}