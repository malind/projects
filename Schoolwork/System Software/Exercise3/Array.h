/*
 ============================================================================
 Name        : Array.h
 Author      : Toni Huovinen, Matti Lindholm
 Description : h-file for the Array.c
 ============================================================================
 */


#ifndef ARRAY_H
#define ARRAY_H


int sumOfElements(int testArray[], int size);

#endif