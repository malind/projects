/*
 ============================================================================
 Name        : Assignment1.c
 Author      : Toni Huovinen, Matti Lindholm
 Description : Practicing the use of multiple files & creating test bench
 ============================================================================
 */

#include <stdio.h>
#include "Prime.h"
#include "Tetranacci.h"
#include "Array.h"

#define SIZE 16

int main() {
	int testArray[SIZE] = {-4, -1, 0, 1, 2, 3, 5, 11, 29, 107, 108, 109, 997, 147312, 547337, 600000}; 
    printf("Sum of elements is %d\n\n", sumOfElements(testArray, SIZE));    	
	for(int i = 0; i <= SIZE - 1; i++){
		if(isTetranacci(testArray[i]) && !checkIfPrime(testArray[i])){
			printf("Number %d\t is Tetranacci but not prime\n", testArray[i]);
		}
	}
	printf("\n");	
	for(int j = 0; j <= SIZE - 1; j++){
		if(checkIfPrime(testArray[j]) && !isTetranacci(testArray[j])){
			printf("Number %d\t is prime but not Tetranacci\n", testArray[j]);
		}
	}
	printf("\n");
	for(int k = 0; k <= SIZE - 1; k++){
		if(checkIfPrime(testArray[k]) && isTetranacci(testArray[k])){
			printf("Number %d\t is prime and Tetranacci\n", testArray[k]);
		}
	}
	return 0;
}