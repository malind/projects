/*
 ============================================================================
 Name        : Tetranacci.h
 Author      : Toni Huovinen, Matti Lindholm
 Description : h-file for the Tetranacci.c
 ============================================================================
 */


#ifndef TETRANACCI_H
#define TETRANACCI_H


int isTetranacci(int valueFromArray);

#endif