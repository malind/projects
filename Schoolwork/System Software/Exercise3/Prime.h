/*
 ============================================================================
 Name        : Prime.h
 Author      : Toni Huovinen, Matti Lindholm
 Description : h-file for the Prime.c
 ============================================================================
 */


#ifndef PRIME_H
#define PRIME_H

int checkIfPrime(int valueFromArray);

#endif