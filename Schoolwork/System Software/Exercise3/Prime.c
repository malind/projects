/*
 ============================================================================
 Name        : Prime.c
 Author      : Toni Huovinen, Matti Lindholm
 Description : This function solves if the value in an array is prime.
 ============================================================================
 */


#include <stdio.h>
#include "Prime.h"

int checkIfPrime(int valueFromArray){
	int checker = 0;
	// First checks if the given number is lower or equal than 1.
	// If it is, checker is 1 and program goes directly to end.
	if(valueFromArray <= 1){
		checker = 1;
	}
	else{
		// No reason to check further than value/2. Numbers above that couldn't divide evenly
		for(int i = 2; i <= valueFromArray/2; i++){		
			if(valueFromArray % i == 0){
				checker = 1;
				break;
			}
		}
	}
	// Returns 0 or 1 depending on the value of checker.
	if(checker == 0){
		return 1;
	}
	else{
		return 0;
	}
}